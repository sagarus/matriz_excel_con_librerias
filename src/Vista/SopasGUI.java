package Vista;
import Negocio.SopaDeLetras;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.scene.layout.*;
import javafx.application.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.embed.swing.*;
import javafx.scene.text.Font;
import javafx.scene.control.Alert.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import java.io.FileOutputStream;

public class SopasGUI extends Application 
{     
    private TextField textField2;
    private Label label0;
    private Label label5;
    private FileChooser fileChooser;
    private Alert alert;
    private Button button1;
    private Button button2;
    SopaDeLetras sopas;

    public SopasGUI(){
        this.go();
    }

    // If used from within the constructor, BlueJ can inspect the application
    public void go() {
        new JFXPanel();
        Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    start(new Stage());
                }
            });
    }

    private Parent getRoot() {

        Pane root = new Pane();
        fileChooser = new FileChooser();
        
        textField2 = new TextField();
        label0 = new Label(); 
        label5 = new Label();
        button1 = new Button();
        button2 = new Button(); 
        alert = new Alert(AlertType.INFORMATION);

        root.getChildren().addAll(textField2,label0,button1,button2,label5);        
        
        textField2.setLayoutX(180);
        textField2.setLayoutY(120);

        label0.setLayoutX(160);
        label0.setLayoutY(10);
        label0.setText("SOPAS");
        label0.setFont(Font.font("Impact",26));
        label0.setMinWidth(90);
        label0.setMinHeight(40);                

        label5.setLayoutX(110);
        label5.setLayoutY(120);
        label5.setText("Palabra");
        label5.setFont(Font.font("Impact",16)); 

        button1.setLayoutX(210);
        button1.setLayoutY(80);
        button1.setText("buscar palabra");
        
        button2.setLayoutX(110);
        button2.setLayoutY(80);
        button2.setText("cargar sopa");     
        
        alert.setTitle("Alerta");
        alert.setHeaderText(null);

        return root;
    }

    @Override
    public void start(Stage primaryStage) {         
        Scene scene = new Scene(getRoot(), 400,200); 
        sopas = new SopaDeLetras ();               
        fileChooser = new FileChooser();  
        
        //manejo del evento del botón crear tabla
        button2.setOnAction(event -> {
             File selectedFile = fileChooser.showOpenDialog(primaryStage);
             try{
             sopas = leerExcel(selectedFile);  
             alert.setContentText("Sopa de letras cargada con éxito. Ingrese la palabra a buscar");
             alert.showAndWait();
             } catch (Exception ex) {
                System.err.println("error, no se pudo cargar la sopa");  
                alert.setContentText("error, no se pudo cargar la sopa. Verifique");
                alert.showAndWait();
            }
        });
        
        

        //manejo del evento del botón buscar palabra
        button1.setOnAction(event -> {      
            try{
                sopas.getContar(textField2.getText());
                crearPdf(sopas);
                alert.setContentText("PDF creado. Verifique en la carpeta del proyecto");
                alert.showAndWait();
            }catch(Exception ex)
            {
                System.err.println("error, no se pudo generar pdf");
                alert.setContentText("error, no se pudo generar pdf. Verifique");
                alert.showAndWait();
            }
            });
            
        primaryStage.setTitle("Sopa de letras");
        primaryStage.setOnCloseRequest(e->System.exit(0));//should be used in BlueJ. Otherwise you have to reset the VM manually after closing
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /* @param args the command line arguments
     */
    public static void main(String[] args) {
        //launch(args);
        new SopasGUI();  
    }
    
    public static SopaDeLetras leerExcel(File file) throws Exception {
        SopaDeLetras s = new SopaDeLetras();
        HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream(file));
        //Obtiene la hoja 1
        HSSFSheet hoja = archivoExcel.getSheetAt(0);
        //Obtiene el número de la última fila con datos de la hoja.
        int canFilas = hoja.getLastRowNum()+1;
        s.asignarCantFilas(canFilas);
        int cantCol = 0;
       
        for (int i = 0; i < canFilas; i++) {
            //Obtiene el índice de la última celda contenida en esta fila MÁS UNO.
            HSSFRow filas = hoja.getRow(i);
            //Obtiene la cantidad de colomunas de esa fila
            cantCol=filas.getLastCellNum();
            s.asignarCantColumnas(i,cantCol);
            
            for(int j=0;j<cantCol;j++)    
            {
                //Obtiene la celda y su valor respectivo
                //double r=filas.getCell(i).getNumericCellValue();
                String valor=filas.getCell(j).getStringCellValue();
                char valorChar = valor.charAt(0);
                s.setValorMatriz(i,j,valorChar);
                
            }
  
       }        
        return s;
    }
    
    public static void crearPdf(SopaDeLetras s) throws Exception
     {
    // Se crea el documento
    
    Document documento = new Document();

    // Se crea el OutputStream para el fichero donde queremos dejar el pdf.
    FileOutputStream ficheroPdf = new FileOutputStream("sopaPdf.pdf");

    // Se asocia el documento al OutputStream y se indica que el espaciado entre
    // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
    PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);

    // Se abre el documento.
    PdfWriter.getInstance(documento, new FileOutputStream("sopaPdf.pdf"));
    documento.open();

    PdfPTable tabla = new PdfPTable(s.getFilas());
    
    
    for (int i = 0; i < s.getFilas(); i++)
    {
        for (int j = 0; j < s.getColumnas(); j++)
    {
        
        if(s.getSopas()[i][j]==s.getNulificador()[i][j] && s.getNulificador()[i][j]!='o' )
        { 
            Paragraph paragraph = new Paragraph("" + s.getSopas()[i][j]);
            PdfPCell cell=new PdfPCell(paragraph);
            cell.setBackgroundColor(BaseColor.YELLOW);
            tabla.addCell(cell);
        }
        else
        tabla.addCell("" + s.getSopas()[i][j]);
        //tabla.getCell(0,0)
       
    }
	
    }
    documento.add(tabla);

    documento.close();
    }
}

