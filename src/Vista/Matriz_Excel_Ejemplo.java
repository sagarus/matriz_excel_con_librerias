/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import Negocio.SopaDeLetras;
import java.io.FileInputStream;
import org.apache.poi.hssf.usermodel.*;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;

/**
 *
 * @author madarme
 */
public class Matriz_Excel_Ejemplo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        SopaDeLetras s = leerExcel();
        
        try
        {        
            System.out.println(s.toString());
            //System.out.println(s.toString2());
            if(s.esCuadrada()==true)
            {
                System.out.println("es cuadrada");
            }
            if(s.esDispersa()==true)
            {
                System.out.println("es dispersa");
            }
            if(s.esRectangular()==true)
            {
                System.out.println("es rectangular");
            }     
            System.out.println(s.getDiagonalPrincipal());          
            
            s.getContar("PAO");

            System.out.println("*******" + "\n");            
        }catch(Exception error){

            System.err.println(error.getMessage());

        }
        
        crearPdf(s);
        
    }
     public static SopaDeLetras leerExcel() throws Exception {
        SopaDeLetras s = new SopaDeLetras();
        HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream("Sopa De Letras.xls"));
        //Obtiene la hoja 1
        HSSFSheet hoja = archivoExcel.getSheetAt(0);
        //Obtiene el número de la última fila con datos de la hoja.
        int canFilas = hoja.getLastRowNum()+1;
        s.asignarCantFilas(canFilas);
        int cantCol = 0;
       
        for (int i = 0; i < canFilas; i++) {
            //Obtiene el índice de la última celda contenida en esta fila MÁS UNO.
            HSSFRow filas = hoja.getRow(i);
            //Obtiene la cantidad de colomunas de esa fila
            cantCol=filas.getLastCellNum();
            s.asignarCantColumnas(i,cantCol);
            
            for(int j=0;j<cantCol;j++)    
            {
                //Obtiene la celda y su valor respectivo
                //double r=filas.getCell(i).getNumericCellValue();
                String valor=filas.getCell(j).getStringCellValue();
                char valorChar = valor.charAt(0);
                s.setValorMatriz(i,j,valorChar);
                
            }
  
       }
        //s.getContar("NO");
        return s;
    }  
     
     public static void crearPdf(SopaDeLetras s) throws Exception
     {
    // Se crea el documento
    
    Document documento = new Document();

    // Se crea el OutputStream para el fichero donde queremos dejar el pdf.
    FileOutputStream ficheroPdf = new FileOutputStream("sopaPdf.pdf");

    // Se asocia el documento al OutputStream y se indica que el espaciado entre
    // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
    PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);

    // Se abre el documento.
    PdfWriter.getInstance(documento, new FileOutputStream("sopaPdf.pdf"));
    documento.open();

    PdfPTable tabla = new PdfPTable(s.getFilas());
    
    
    for (int i = 0; i < s.getFilas(); i++)
    {
        for (int j = 0; j < s.getColumnas(); j++)
    {
        
        if(s.getSopas()[i][j]==s.getNulificador()[i][j] && s.getNulificador()[i][j]!='o' )
        { 
            Paragraph paragraph = new Paragraph("" + s.getSopas()[i][j]);
            PdfPCell cell=new PdfPCell(paragraph);
            cell.setBackgroundColor(BaseColor.YELLOW);
            tabla.addCell(cell);
        }
        else
        tabla.addCell("" + s.getSopas()[i][j]);
        //tabla.getCell(0,0)
       
    }
	
    }
    documento.add(tabla);
    
     
            
            // Left
            Paragraph paragraph = new Paragraph(s.getContar("NO"));
            documento.add(paragraph);

    documento.close();
    }
}
